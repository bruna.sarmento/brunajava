package classes;

public class itemDeMostruario {
    private float desconto;
    private String dataUpload;
    private String dataVenda;


    public itemDeMostruario(float desconto, String dataUpload, String dataVenda) {
        this.setDescontos(desconto);
        this.setDataUpload(dataUpload);
        this.setDataVenda(dataVenda);
    }


    public void setDescontos(float desconto) {
        this.desconto = desconto;
    }
    public float getDescontos() {
        return this.desconto;
    }
    public void setDataUpload(String dataUpload) {
        this.dataUpload = dataUpload;
    }
    public String getDataUpload() {
        return this.dataUpload;
    }
    public void setDataVenda(String dataVenda) {
        this.dataVenda = dataVenda;
    }
    public String getDataVenda() {
        return this.dataVenda;
    }

    
}